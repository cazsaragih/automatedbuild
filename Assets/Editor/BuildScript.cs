﻿using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;

public class BuildScript
{
    [MenuItem("Build/Build Android")]
    static void Build()
    {
        var scenes = EditorBuildSettings.scenes;
        BuildReport report = BuildPipeline.BuildPlayer(scenes, "Builds/test.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);
        BuildSummary summary = report.summary;

        if (summary.result == BuildResult.Succeeded)
        {
            BuildAssetBundle();
        }
    }

    static void BuildAssetBundle()
    {
        BuildPipeline.BuildAssetBundles("AssetBundles", BuildAssetBundleOptions.None, BuildTarget.Android);
    }
}
