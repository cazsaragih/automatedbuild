import subprocess
import shutil
import os
import zipfile
import threading
import git

print('Pulling latest commit from origin/master')

g = git.Git('"D:\\Unity Projects\\AutomatedBuild"')
g.pull('origin', 'master')

print('Cleaning up Builds directory')

shutil.rmtree('Builds', ignore_errors=True)

print('Cleaning up AssetBundles directory')

shutil.rmtree('AssetBundles', ignore_errors=True)

print ('Creating AssetBundles directory')

os.mkdir("AssetBundles")

print('Starting build process')

UNITY = '"C:\\Program Files\\Unity\\Editor\\2018.4.31f1\\Editor\\Unity.exe"'

subprocess.run(
    f'{UNITY} -quit -projectPath -batchmode {os.getcwd()} -executeMethod BuildScript.Build')

print('Build process ended')